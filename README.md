This code is being used to create the UK 2011 Output Area Classification from the Office for National Statistics.

For further information, get in touch with [Chris Gale](mailto:chris.gale@ucl.ac.uk) or [Alex Singleton](mailto:alex.singleton@liverpool.ac.uk).